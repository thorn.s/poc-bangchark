<?xml version="1.0"?>
<doc>
    <assembly>
        <name>OpenTelemetry.Exporter.InMemory</name>
    </assembly>
    <members>
        <member name="M:OpenTelemetry.Trace.InMemoryExporterHelperExtensions.AddInMemoryExporter(OpenTelemetry.Trace.TracerProviderBuilder,System.Collections.Generic.ICollection{System.Diagnostics.Activity})">
            <summary>
            Adds InMemory exporter to the TracerProvider.
            </summary>
            <param name="builder"><see cref="T:OpenTelemetry.Trace.TracerProviderBuilder"/> builder to use.</param>
            <param name="exportedItems">Collection which will be populated with the exported Activity.</param>
            <returns>The instance of <see cref="T:OpenTelemetry.Trace.TracerProviderBuilder"/> to chain the calls.</returns>
        </member>
        <member name="T:OpenTelemetry.Metrics.InMemoryExporterMetricsExtensions">
            <summary>
            Extension methods to simplify registering of the InMemory exporter.
            </summary>
        </member>
        <member name="M:OpenTelemetry.Metrics.InMemoryExporterMetricsExtensions.AddInMemoryExporter(OpenTelemetry.Metrics.MeterProviderBuilder,System.Collections.Generic.ICollection{OpenTelemetry.Metrics.Metric})">
            <summary>
            Adds InMemory metric exporter to the <see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> using default options.
            </summary>
            <remarks>
            Be aware that <see cref="T:OpenTelemetry.Metrics.Metric"/> may continue to be updated after export.
            </remarks>
            <param name="builder"><see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> builder to use.</param>
            <param name="exportedItems">Collection which will be populated with the exported <see cref="T:OpenTelemetry.Metrics.Metric"/>.</param>
            <returns>The instance of <see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> to chain the calls.</returns>
        </member>
        <member name="M:OpenTelemetry.Metrics.InMemoryExporterMetricsExtensions.AddInMemoryExporter(OpenTelemetry.Metrics.MeterProviderBuilder,System.Collections.Generic.ICollection{OpenTelemetry.Metrics.Metric},System.Action{OpenTelemetry.Metrics.MetricReaderOptions})">
            <summary>
            Adds InMemory metric exporter to the <see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/>.
            </summary>
            <remarks>
            Be aware that <see cref="T:OpenTelemetry.Metrics.Metric"/> may continue to be updated after export.
            </remarks>
            <param name="builder"><see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> builder to use.</param>
            <param name="exportedItems">Collection which will be populated with the exported <see cref="T:OpenTelemetry.Metrics.Metric"/>.</param>
            <param name="configureMetricReader"><see cref="T:OpenTelemetry.Metrics.MetricReader"/> configuration options.</param>
            <returns>The instance of <see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> to chain the calls.</returns>
        </member>
        <member name="M:OpenTelemetry.Metrics.InMemoryExporterMetricsExtensions.AddInMemoryExporter(OpenTelemetry.Metrics.MeterProviderBuilder,System.Collections.Generic.ICollection{OpenTelemetry.Metrics.MetricSnapshot})">
            <summary>
            Adds InMemory metric exporter to the <see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> using default options.
            The exporter will be setup to export <see cref="T:OpenTelemetry.Metrics.MetricSnapshot"/>.
            </summary>
            <remarks>
            Use this if you need a copy of <see cref="T:OpenTelemetry.Metrics.Metric"/> that will not be updated after export.
            </remarks>
            <param name="builder"><see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> builder to use.</param>
            <param name="exportedItems">Collection which will be populated with the exported <see cref="T:OpenTelemetry.Metrics.Metric"/> represented as <see cref="T:OpenTelemetry.Metrics.MetricSnapshot"/>.</param>
            <returns>The instance of <see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> to chain the calls.</returns>
        </member>
        <member name="M:OpenTelemetry.Metrics.InMemoryExporterMetricsExtensions.AddInMemoryExporter(OpenTelemetry.Metrics.MeterProviderBuilder,System.Collections.Generic.ICollection{OpenTelemetry.Metrics.MetricSnapshot},System.Action{OpenTelemetry.Metrics.MetricReaderOptions})">
            <summary>
            Adds InMemory metric exporter to the <see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/>.
            The exporter will be setup to export <see cref="T:OpenTelemetry.Metrics.MetricSnapshot"/>.
            </summary>
            <remarks>
            Use this if you need a copy of <see cref="T:OpenTelemetry.Metrics.Metric"/> that will not be updated after export.
            </remarks>
            <param name="builder"><see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> builder to use.</param>
            <param name="exportedItems">Collection which will be populated with the exported <see cref="T:OpenTelemetry.Metrics.Metric"/> represented as <see cref="T:OpenTelemetry.Metrics.MetricSnapshot"/>.</param>
            <param name="configureMetricReader"><see cref="T:OpenTelemetry.Metrics.MetricReader"/> configuration options.</param>
            <returns>The instance of <see cref="T:OpenTelemetry.Metrics.MeterProviderBuilder"/> to chain the calls.</returns>
        </member>
        <member name="T:OpenTelemetry.Metrics.MetricSnapshot">
            <summary>
            This class represents a selective copy of <see cref="T:OpenTelemetry.Metrics.Metric"/>.
            This contains the minimum fields and properties needed for most
            unit testing scenarios.
            </summary>
        </member>
        <member name="T:OpenTelemetry.Internal.Guard">
            <summary>
            Methods for guarding against exception throwing values.
            </summary>
        </member>
        <member name="M:OpenTelemetry.Internal.Guard.ThrowIfNull(System.Object,System.String)">
            <summary>
            Throw an exception if the value is null.
            </summary>
            <param name="value">The value to check.</param>
            <param name="paramName">The parameter name to use in the thrown exception.</param>
        </member>
        <member name="M:OpenTelemetry.Internal.Guard.ThrowIfNullOrEmpty(System.String,System.String)">
            <summary>
            Throw an exception if the value is null or empty.
            </summary>
            <param name="value">The value to check.</param>
            <param name="paramName">The parameter name to use in the thrown exception.</param>
        </member>
        <member name="M:OpenTelemetry.Internal.Guard.ThrowIfNullOrWhitespace(System.String,System.String)">
            <summary>
            Throw an exception if the value is null or whitespace.
            </summary>
            <param name="value">The value to check.</param>
            <param name="paramName">The parameter name to use in the thrown exception.</param>
        </member>
        <member name="M:OpenTelemetry.Internal.Guard.ThrowIfZero(System.Int32,System.String,System.String)">
            <summary>
            Throw an exception if the value is zero.
            </summary>
            <param name="value">The value to check.</param>
            <param name="message">The message to use in the thrown exception.</param>
            <param name="paramName">The parameter name to use in the thrown exception.</param>
        </member>
        <member name="M:OpenTelemetry.Internal.Guard.ThrowIfInvalidTimeout(System.Int32,System.String)">
            <summary>
            Throw an exception if the value is not considered a valid timeout.
            </summary>
            <param name="value">The value to check.</param>
            <param name="paramName">The parameter name to use in the thrown exception.</param>
        </member>
        <member name="M:OpenTelemetry.Internal.Guard.ThrowIfOutOfRange(System.Int32,System.String,System.Int32,System.Int32,System.String,System.String,System.String)">
            <summary>
            Throw an exception if the value is not within the given range.
            </summary>
            <param name="value">The value to check.</param>
            <param name="paramName">The parameter name to use in the thrown exception.</param>
            <param name="min">The inclusive lower bound.</param>
            <param name="max">The inclusive upper bound.</param>
            <param name="minName">The name of the lower bound.</param>
            <param name="maxName">The name of the upper bound.</param>
            <param name="message">An optional custom message to use in the thrown exception.</param>
        </member>
        <member name="M:OpenTelemetry.Internal.Guard.ThrowIfOutOfRange(System.Double,System.String,System.Double,System.Double,System.String,System.String,System.String)">
            <summary>
            Throw an exception if the value is not within the given range.
            </summary>
            <param name="value">The value to check.</param>
            <param name="paramName">The parameter name to use in the thrown exception.</param>
            <param name="min">The inclusive lower bound.</param>
            <param name="max">The inclusive upper bound.</param>
            <param name="minName">The name of the lower bound.</param>
            <param name="maxName">The name of the upper bound.</param>
            <param name="message">An optional custom message to use in the thrown exception.</param>
        </member>
        <member name="M:OpenTelemetry.Internal.Guard.ThrowIfNotOfType``1(System.Object,System.String)">
            <summary>
            Throw an exception if the value is not of the expected type.
            </summary>
            <param name="value">The value to check.</param>
            <param name="paramName">The parameter name to use in the thrown exception.</param>
            <typeparam name="T">The type attempted to convert to.</typeparam>
            <returns>The value casted to the specified type.</returns>
        </member>
        <member name="T:System.Runtime.CompilerServices.CallerArgumentExpressionAttribute">
            <summary>
            Allows capturing of the expressions passed to a method.
            </summary>
        </member>
        <member name="M:System.Runtime.CompilerServices.CallerArgumentExpressionAttribute.#ctor(System.String)">
            <summary>
            Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CallerArgumentExpressionAttribute"/> class.
            </summary>
            <param name="parameterName">The name of the targeted parameter.</param>
        </member>
        <member name="P:System.Runtime.CompilerServices.CallerArgumentExpressionAttribute.ParameterName">
            <summary>
            Gets the target parameter name of the CallerArgumentExpression.
            </summary>
        </member>
    </members>
</doc>
