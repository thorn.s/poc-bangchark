﻿using OpenTelemetry.Resources;
using OpenTelemetry;
using OpenTelemetry.Trace;
using System;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using System.Net.Http;
using System.Security.Policy;
using System.Threading.Tasks;
using OpenTelemetry.Exporter;
using OpenTelemetry.Metrics;

namespace SampleWebApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private IDisposable tracerProvider;
        private IDisposable meterProvider;

        protected void Application_Start()
        {
            var builder = Sdk.CreateTracerProviderBuilder()
                 .AddAspNetInstrumentation((options) => options.Enrich
        = (activity, eventName, rawObject) =>
        {
            if (eventName.Equals("OnStartActivity"))
            {
                if (rawObject is HttpRequest httpRequest)
                {
                    activity?.SetTag("physicalPath", httpRequest.PhysicalPath);
                }
            }
            else if (eventName.Equals("OnStopActivity"))
            {
                if (rawObject is HttpResponse httpResponse)
                {
                    activity?.SetTag("responseType", httpResponse.ContentType);
                }
            }
        })
                 .AddHttpClientInstrumentation();
                 //.AddGrpcClientInstrumentation();

            switch (ConfigurationManager.AppSettings["UseExporter"].ToLowerInvariant())
            {
                case "otlp":
                    builder.AddOtlpExporter(otlpOptions =>
                    {
                        otlpOptions.Protocol = OtlpExportProtocol.HttpProtobuf;
                        otlpOptions.Endpoint = new Uri(ConfigurationManager.AppSettings["OtlpEndpoint"]);
                    });
                    break;
                default:
                    builder.AddConsoleExporter(options => options.Targets = ConsoleExporterOutputTargets.Debug);
                    break;
            }

            this.tracerProvider = builder.Build();

            // Metrics
            // Note: Tracerprovider is needed for metrics to work
            // https://github.com/open-telemetry/opentelemetry-dotnet/issues/2994

            var meterBuilder = Sdk.CreateMeterProviderBuilder()
                 .AddAspNetInstrumentation();

            switch (ConfigurationManager.AppSettings["UseMetricsExporter"].ToLowerInvariant())
            {
                case "otlp":
                    meterBuilder.AddOtlpExporter(otlpOptions =>
                    {
                        otlpOptions.Endpoint = new Uri(ConfigurationManager.AppSettings["OtlpEndpoint"]);
                    });
                    break;
                default:
                    meterBuilder.AddConsoleExporter((exporterOptions, metricReaderOptions) =>
                    {
                        exporterOptions.Targets = ConsoleExporterOutputTargets.Debug;
                    });
                    break;
            }

            this.meterProvider = meterBuilder.Build();

            GlobalConfiguration.Configure(WebApiConfig.Register);

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_End()
        {
            this.tracerProvider?.Dispose();
            this.meterProvider?.Dispose();
        }
    }
}
    
